from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from routers import users, workouts
from authenticator import authenticator


app = FastAPI()


app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(authenticator.router, tags=["auth"])
app.include_router(users.router, tags=["users"])
app.include_router(workouts.router, tags=["workouts"])
