from authenticator import authenticator
from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    Request,
    Response,
    status
)
from queries.workouts import WorkoutsQueries
from typing import (
    List,
    Optional,
    Union,
)
from models.models import (
    Error,
    WorkoutForm,
    WorkoutIn,
    WorkoutOut,
    LeaderboardOut,
)


router = APIRouter()


@router.post("/api/workouts", response_model=Union[WorkoutOut, Error])
def create_workout(
    workout_form: WorkoutForm,
    request: Request,
    response: Response,
    repo: WorkoutsQueries = Depends(),
    user_data: Optional[dict] = Depends(
        authenticator.get_current_account_data
    ),
):
    user_id = user_data["id"]
    is_completed = workout_form.check_completion()
    workout = WorkoutIn(
        user_id=user_id,
        is_completed=is_completed,
        **workout_form.dict()
    )
    try:
        new_workout = repo.create(workout)
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create a workout with provided info: " + str(e),
        )
    return new_workout


@router.get(
        "/api/workouts/leaderboard",
        response_model=Union[List[LeaderboardOut], Error]
)
def get_leaderboard(
    repo: WorkoutsQueries = Depends(),
    user_data: Optional[dict] = Depends(
        authenticator.get_current_account_data)
):
    return repo.get_leaderboard()


@router.get(
        "/api/workouts/mine",
        response_model=Union[Error, List[WorkoutOut]]
)
def get_all_user_workouts(
    repo: WorkoutsQueries = Depends(),
    user_data: Optional[dict] = Depends(
        authenticator.get_current_account_data)
):
    return repo.get_all_user_workouts(user_data["id"])


@router.get("/api/workouts/{id}", response_model=Optional[WorkoutOut])
def get_single_workout_by_id(
    id: int,
    repo: WorkoutsQueries = Depends(),
    user_data: Optional[dict] = Depends(
        authenticator.get_current_account_data
        ),
):
    workout = repo.get_single_workout_by_id(id, user_data["id"])
    if workout:
        return workout
