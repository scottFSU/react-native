import { getToken } from "@galvanize-inc/jwtdown-for-react"
import { API_HOST } from "../main"


const loginUser = async (username, password) => {
    let userToken = null;

    const url = `${API_HOST}/token`;
    const form = new FormData();
    form.append("username", username);
    form.append("password", password);
    const response = await fetch(url, {
        method: "post",
        credentials: "include",
        body: form,
    })
    if (response.ok){
        userToken = await getToken(API_HOST);
    }

    return userToken;
}

export default loginUser
