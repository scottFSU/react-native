# Daily Journal

## 18 Jan 2024
Pretty good day today. We got through database setup pretty smoothly. Then stubbed out all of our api endpoints, and created most of the pydantic models we'll need for tackling auth. We ultimately didn't get to start on auth today, but we made steady progress, and I think we'll put a big dent into auth tomorrow.

Some of my main contributions today:
1. Fixed a bunch of issues with our docker-compose file so that our fastapi service would actually await the db.
2. Helped troubleshoot issues with the creation of our migrations tables.
3. Stubbed out our first api endpoint, so I created a bunch of the directory structure, imports, etc.

## 17 Jan 2024
Today was a pretty tough day, to be honest. We had a great standup, aligning on a plan for the day that seemed ambitious but which I had confidence that we would put a dent in. But in one of our "warmup" tasks, we ran into a gnarly bug with psycopg that ended up taking >5 hours to resolve.

So what did I learn today?
1. Yet again, I learn the lesson: be iterative. We shouldn't have changed our usernames and passwords AND done the `.env` file config at the same time. We should have set up the `.env` file with the existing credentials first, and only then changed things. Make a tiny change, then test. Make a tiny change, then test. If we had done this, it's _possible_ that we would have figured out sooner that the issue had to do with our password.

2. Obviously, I learned that I need to be careful with postgres database passwords. No @s allowed.

Tomorrow's another day. Hopefully we'll tackle auth. I'm excited to take that on :)
